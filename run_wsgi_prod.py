# -*- coding: utf-8 -*-

"""
Wersja Produkcyjna.

# Przydatne polecenia dla terminala
- uruchomienie aplikacji -> /home/adrosar/venv/bin/uwsgi --http 127.0.0.1:35421 --chdir /home/adrosar/domains/adrosar.linuxpl.eu/python_app/ --wsgi-file run_wsgi_prod.py --master --processes 1 --workers 1 --threads 1 --daemonize=/home/adrosar/domains/adrosar.linuxpl.eu/logs/python_app.txt
- wyswietlenie wszystkich procesów -> ps aux
- wyłączenie (zabicie) aplikacji -> kill -9 PID1 PID2 PID3


# Przekierowania:
- Adres .......: http://adrosar.linuxpl.eu/web
  -> Na katalog: /home/adrosar/domains/adrosar.linuxpl.eu/public_html
- Adres .......: http://adrosar.linuxpl.eu
  -> Na IP:port: 127.0.0.1:35421

# Inne informacje:
- IP Servera: 136.243.44.221
"""

import sys

sys.path.insert(0, "/home/adrosar/domains/adrosar.linuxpl.eu/python_app/")


# => Konfiguracja:
from app.config import set_cfg, get_cfg

set_cfg("full_path_root", "/home/adrosar/domains/adrosar.linuxpl.eu/python_app/")
set_cfg("full_url_site", "http://adrosar.linuxpl.eu")
set_cfg("assets_dir", "assets/")
set_cfg("scripts_dir", "scripts/")
set_cfg("upload_dir", "upload/")

set_cfg("assets_url", get_cfg("full_url_site") + "/" + get_cfg("assets_dir"))

set_cfg("scripts_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir"))
set_cfg("vendors_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir") + "vendors/")
set_cfg("js_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir") + "js/")
set_cfg("css_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir") + "css/")

set_cfg("upload_url", get_cfg("full_url_site") + "/" + get_cfg("upload_dir"))


# Ciche wyjątki - Gdy jest TRUE wyjątki nie będą wyświetlały błędu ani przerywały działania aplikacji. Pojawi się tylko wpis w logu konsoli o wystąpieniu wyjątku i jego numer identyfikacyjny.
set_cfg("silent_exceptions", False)

set_cfg("db_host", "")
set_cfg("db_port", "")
set_cfg("db_user", "")
set_cfg("db_pass", "")
set_cfg("db_name", "")

set_cfg("key_session", u"""`E8!FęĘ\9QPł}&?L~aLŹ9"ćCqc[Kdbą]ą0pR}vsIbCN!['gęEuy`!99%3ŁYń2"!5""")
set_cfg("key_password", u"""ĆĄ>w0C|vK&DSwźńRB$w-=0WźżbNlZi9Ew9=]zlćI1SwŹB:=ep6[Zp#ńVŹŃvR3gŹ9""")
set_cfg("key_data", u""">vq(ś)KMf~p#Eń!ĄąL}?pł?EĄŁo{vA-bhdUŃĄŚj*}%ęŹA`(ąŁĆŻ+]MŃ]Vć'hnłó3""")
set_cfg("key_cookie", u"""3BĄ3{<kŁfLA?ó[Ó8?9#e8ż=tj7`D4kn{^#hł)lnŹ!Ge!p<s7O'Gc=ĘCl^*r.%Mł!""")

set_cfg('access_code_list', [
    '2zpQPCqje4tI8eM5',
    'wCqvA!pDORgdJaDd',
    'ePO3qv2pFd2CX9!e',
    'wwzCdq&!NgHEVB9c',
    'NKqzUgAB8q4TFORm',
    'mumAKBgbtuEa58Mh',
    '$bNGzASJx&RupIw#',
    'GP54ygtwd#CN%hUe',
    'qIQv!GNxbfSFPNwH',
    'hgKfTOPOHu66v&9a'
])

set_cfg("debug", False)

# =/ Konfiguracja.


# => Procedura uruchomienia aplikacji:

import logging

sys.stdout = sys.stderr
logging.basicConfig(stream=sys.stderr)

import app.engine
import app.routes

app.engine.flask_app.debug = False
application = app.engine.flask_app

# =/ Procedura uruchomienia aplikacji.