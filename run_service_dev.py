# -*- coding: utf-8 -*-

# = Konfiguracja =>
from app.config import set_cfg, get_cfg

set_cfg("full_path_root", "C:/App/WebServ/httpd/portfolio/")
set_cfg("full_url_site", "https://127.0.0.1:5000")
set_cfg("assets_dir", "assets/")
set_cfg("scripts_dir", "scripts/")
set_cfg("upload_dir", "upload/")

set_cfg("assets_url", get_cfg("full_url_site") + "/" + get_cfg("assets_dir"))

set_cfg("scripts_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir"))
set_cfg("vendors_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir") + "vendors/")
set_cfg("js_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir") + "js/")
set_cfg("css_url", get_cfg("full_url_site") + "/" + get_cfg("scripts_dir") + "css/")

set_cfg("upload_url", get_cfg("full_url_site") + "/" + get_cfg("upload_dir"))


# Ciche wyjątki - Gdy jest TRUE wyjątki nie będą wyświetlały błędu ani przerywały działania aplikacji. Pojawi się tylko wpis w logu konsoli o wystąpieniu wyjątku i jego numer identyfikacyjny.
set_cfg("silent_exceptions", False)

set_cfg("db_host", "")
set_cfg("db_port", "")
set_cfg("db_user", "")
set_cfg("db_pass", "")
set_cfg("db_name", "")

set_cfg("key_session", u"""`E8!FęĘ\9QPł}&?L~aLŹ9"ćCqc[Kdbą]ą0pR}vsIbCN!['gęEuy`!99%3ŁYń2"!5""")
set_cfg("key_password", u"""ĆĄ>w0C|vK&DSwźńRB$w-=0WźżbNlZi9Ew9=]zlćI1SwŹB:=ep6[Zp#ńVŹŃvR3gŹ9""")
set_cfg("key_data", u""">vq(ś)KMf~p#Eń!ĄąL}?pł?EĄŁo{vA-bhdUŃĄŚj*}%ęŹA`(ąŁĆŻ+]MŃ]Vć'hnłó3""")
set_cfg("key_cookie", u"""3BĄ3{<kŁfLA?ó[Ó8?9#e8ż=tj7`D4kn{^#hł)lnŹ!Ge!p<s7O'Gc=ĘCl^*r.%Mł!""")

set_cfg('access_code_list', ['pass'])

set_cfg("debug", True)

# = Konfiguracja =/


# => Procedura uruchomienia aplikacji:
import sys
from app.config import get_cfg

sys.path.insert(0, get_cfg("full_path_root"))

print "\nPYTHON PATH:"
for path in sys.path:
    print "\t" + str(path)

print "\n"

import app.certificate
import app.engine
import app.routes

app.engine.flask_app.debug = True
app.engine.flask_app.run(
    host='0.0.0.0',
    port=5000,
    ssl_context=app.certificate.ssl_context
)

# =/ Procedura uruchomienia aplikacji.