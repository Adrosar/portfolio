(function (_this, _factory) {
    "use strict";

    if (typeof _this.waitFor === "undefined") {
        _this.waitFor = _factory();
    }

})(this, function () {
    "use strict";

    function waitFor(_init, _test, _result) {

        var data = {
            errorNo: 0,
            delay: 0,
            steps: 1,
            ok: false,
            mode: 1
        };

        _init(data);

        function isReady() {

            if (_test()) {
                data.ok = true;
                _result(data);
                return;
            } else {
                data.ok = false;
            }

            if (data.errorNo == data.steps) {

                if (data.mode == 2) {
                    data.ok = false;
                    _result(data);
                    return;
                }

            } else if (data.errorNo < data.steps) {

                if (data.mode == 3) {
                    _result(data);
                }

                data.errorNo++;
                setTimeout(isReady, data.delay);
            }

            return;
        }

        setTimeout(isReady, data.delay);
    }

    return waitFor;
});