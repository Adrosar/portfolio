(function (_this, _factory) {
    "use strict";
    if (typeof _this.loadScript === "undefined") {
        _this.loadScript = _factory();
    }
})(this, function () {
    "use strict";
    var w = window;
    var d = w.document;

    var scripts = [];

    function createScript(_src) {
        var script = d.createElement("SCRIPT");
        script.type = "text/javascript";
        script.async = "async";
        script.src = _src;
        return script;
    }

    // KLASA ScriptClass

    function ScriptClass(_scr, _onLoad) {
        var This = this;

        var script = createScript(_scr);
        script.addEventListener('load', _onLoad);

        d.body.appendChild(script);
    }

    function init(_scr, _onLoad) {
        new ScriptClass(_scr, _onLoad);
    }

    return init;
});