define(['js/mail-decryption-all'], function (_allMailDecrypt) {
    "use strict";

    // Aliasy:
    var W = window;
    var D = W.document;
    var B = D.body;

    // Zmienne i obiekty:
    var $menuBtts = $('.menu-btt');
    var $bttTab1 = $('#bttTab1');
    var $bttTab2 = $('#bttTab2');
    var $bttTab3 = $('#bttTab3');
    var $bttTab4 = $('#bttTab4');
    var $B = $(B);


    // Funkcje:
    function scrollToAnchor(_aid) {
        var $aTag = $("a[name='" + _aid + "']");

        setTimeout(function () {
            $B.animate({scrollTop: $aTag.offset().top}, 'slow');
        }, 200);
    }


    function goTo(_url) {
        switch (_url) {
            case '#wyksztalcenie':
                $bttTab2.trigger('click');
                scrollToAnchor('wyksztalcenie');
                break;
            case '#praca':
                $bttTab2.trigger('click');
                scrollToAnchor('praca');
                break;
            case '#umiejetnosci':
                $bttTab3.trigger('click');
                scrollToAnchor('umiejetnosci');
                break;
            case '#piaskownica-1':
                $bttTab4.trigger('click');
                scrollToAnchor('piaskownica-1');
                break;
            case '#projekty':
                alert("Przepraszam ale dział jest obecnie w budowie. Zapraszam do zobaczenia konta BitBucket oraz profilu na LinkedIn (linki po lewej stronie w menu) Tam zobaczycie mój kod i projekty w których uczestniczyłem i o których mogę pisać ;)");
                break;
            default:
            // ...
        }
    }

    // Akcje:
    $menuBtts.each(function (_index, _node) {
        var $node = $(_node);
        var url = $node.attr('href');

        $node.click(function (_event) {
            goTo(url);
            _event.preventDefault();
            return false;
        });

    });

    _allMailDecrypt('encrypted-mail');

});