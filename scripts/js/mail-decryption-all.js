define(function () {
    "use strict";

    function allMailDecrypt(_class) {
        $('.' + _class).each(function (_index, _node) {
            var data_str = _node.getAttribute('href');
            data_str = data_str.substring(2);
            var data = data_str.split(',');

            if (typeof atob === 'function') {

                var name = atob(data[0]);
                var email = atob(data[1]) + '@' + atob(data[2]);

                if (name === '%NoNe%') {
                    name = email;
                }

                _node.href = 'mailto:' + email;

                var label = document.createTextNode(name);
                _node.appendChild(label);
            }
        });
    }

    return allMailDecrypt;

});