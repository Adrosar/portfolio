# -*- coding: utf-8 -*-

if __name__ == "__main__":
    exit("EXIT")

# --- -- -

cfg = dict()


def set_cfg(_key, _val):
    global cfg
    cfg[_key] = _val
    pass


def get_cfg(_key):
    global cfg
    return cfg[_key]