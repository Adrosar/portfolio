# -*- coding: utf-8 -*-


import base64


def encrypt(_email, _name=None, _class='encrypted-mail'):
    E = _email.split('@')  # [0] -> nazwa, [1] -> server.domena

    if (_name == None):
        _name = '%NoNe%'

    name = base64.b64encode(_name.encode('utf8'))

    e_name = base64.b64encode(E[0])
    e_domain = base64.b64encode(E[1])

    _email = name + ',' + e_name + ',' + e_domain

    return '<a class="' + _class + '" href="#@' + _email + '"></a>'