# -*- coding: utf-8 -*-


import re


def test(data, data_type=None, len_min=None, len_max=None, pattern=None):
    error_no = 0
    data_len = len(data)
    current_type = type(data)

    if current_type is not str:
        try:
            data = str(data)
        except Exception:
            return False

    if len_min is not None:
        if data_len < len_min:
            error_no += 1

    if len_max is not None:
        if data_len > len_max:
            error_no += 1

    if pattern is not None:
        m = re.match(pattern)

        try:
            mg0 = m.group(0)
        except Exception:
            error_no += 1

    if data_type is not None:

        pass


    # Wynik sprawdzania:
    if error_no == 0:
        return True
    else:
        return False