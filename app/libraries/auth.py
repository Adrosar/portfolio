# -*- coding: utf-8 -*-

if __name__ == "__main__":
    exit("EXIT")

# --- -- -

from flask import session, redirect, url_for
from app.config import get_cfg

# --- -- -


def is_access():
    if 'access' in session:
        if session['access'] in get_cfg('access_code_list'):
            return True

    return False


# --- -- -


def access(f):
    def contener(*args, **kwds):
        if 'access' in session:
            if session['access'] in get_cfg('access_code_list'):
                return f(*args, **kwds)

        return redirect(url_for('lockscreen_run'))

    return contener

