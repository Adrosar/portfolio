# -*- coding: utf-8 -*-

import json


class JavaScript():
    script_begin = "<script type='text/javascript'>"
    script_var = "var g = window.g = window.g || {};"
    script_end = "</script>"

    def __init__(self):
        self.var = dict()
        pass
        # (END) __init__

    def add_global_var(self, _key, _val):
        self.var[_key] = _val
        pass
        # (END) add_global_var

    def render_script_tag(self):
        js_data = ""
        js_data += self.script_begin + "\n" + self.script_var

        for key in self.var.keys():
            json_data = json.dumps(self.var[key])
            js_data += "\n" + "g['" + key + "'] = " + json_data + ";"

        js_data += "\n" + self.script_end
        return js_data
        # (END) render_script_tag

    def render_script(self):
        js_data = ""
        js_data += self.script_var

        for key in self.var.keys():
            json_data = json.dumps(self.var[key])
            js_data += "\n" + "g['" + key + "'] = " + json_data + ";"

        return js_data
        # (END) render_script

    pass
    # (END) JavaScript

