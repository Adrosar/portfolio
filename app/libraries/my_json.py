# -*- coding: utf-8 -*-

import json
from flask import Response


class JSON():
    def __init__(self):
        pass
        # (END) __init__

    @staticmethod
    def response(_obj):
        jsonData = json.dumps(_obj)
        return Response(jsonData, mimetype="text/plain")

    # (END) render

    pass
    # (END) JSON

