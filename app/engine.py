# -*- coding: utf-8 -*-

if __name__ == "__main__":
    exit("EXIT")

# --- -- -

from flask import Flask, session
from app.config import get_cfg
from app.libraries.mail import encrypt as mail_encrypt

# --- -- -

flask_app = Flask(
    __name__,
    template_folder=get_cfg('full_path_root') + "app/templates"
)

flask_app.session_cookie_name = "_sSs_"  # _sSs_, _iIi_, _pPp_, _cCc_
flask_app.secret_key = get_cfg("key_session")
flask_app.jinja_env.globals[''] = ''

# --- -- -


@flask_app.context_processor
def utility_processor():
    return dict(get_cfg=get_cfg, mail_encrypt=mail_encrypt)

