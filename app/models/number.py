# -*- coding: utf-8 -*-

class Number():
    def __init__(self):
        pass

    @classmethod
    def to_float(cls, _text):

        _text = _text.replace(',', '.')

        try:
            result = float(_text)
        except Exception, error:
            print 'Exception 1435833190:', error
            return False, 1435833190
        else:
            return True, result  # => True

    @classmethod
    def to_int(cls, _text):

        try:
            result = int(_text)
        except Exception, error:
            print 'Exception 1435834322:', error
            return False, 1435834322
        else:
            return True, result  # => True