# -*- coding: utf-8 -*-

if __name__ == "__main__":
    exit("EXIT")

# --- -- -

from flask import redirect, url_for

from app.engine import flask_app

from app.controllers.file import get_asset, get_script

from app.controllers.webcv import webcv_run
from app.controllers.lockscreen import lockscreen_run
from app.controllers.logout import logout_run

# --- -- - Funkcja ułatwiająca dodawanie adresów URL:

def add_route(*args, **options):
    flask_app.add_url_rule(*args, **options)


# --- -- - Obsługa plików statycznych:

add_route('/assets/<path:_path>', 'get_file_asset', get_asset)
add_route('/scripts/<path:_path>', 'get_file_script', get_script)

# --- -- - Adresy stron:

add_route('/', 'lockscreen_run', lockscreen_run, methods=['GET', 'POST'])
add_route('/webcv', 'webcv_run', webcv_run, methods=['GET', 'POST'])
add_route('/logout', 'logout_run', logout_run, methods=['GET'])

# --- -- - Każdy zły adres URL przenosi do strony logowania:

@flask_app.route('/', defaults={'path': ''})
@flask_app.route('/<path:path>')
def catch_all(path):
    return redirect(url_for('lockscreen_run'))
