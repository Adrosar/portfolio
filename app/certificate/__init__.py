# -*- coding: utf-8 -*-

from app.config import get_cfg

ssl_context = (
    get_cfg("full_path_root") + 'app/certificate/localhost.crt',
    get_cfg("full_path_root") + 'app/certificate/localhost.key'
)