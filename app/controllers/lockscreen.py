# -*- coding: utf-8 -*-

from flask import render_template, request, redirect, url_for, session
from app.config import get_cfg


def lockscreen_run():
    try:
        if request.method == 'POST':
            if 'sign-in' in request.form:
                if request.form['sign-in'] == 'run':
                    if request.form['access-code'] in get_cfg('access_code_list'):
                        session['access'] = request.form['access-code']
                        return redirect(url_for('webcv_run'))
    except Exception:
        pass

    return render_template('pages/lockscreen.tpl')