# -*- coding: utf-8 -*-

from flask import redirect, url_for, session


def logout_run():
    session.pop('access', None)
    return redirect(url_for('lockscreen_run'))