# -*- coding: utf-8 -*-

"""
...
"""

if __name__ == "__main__":
    exit("EXIT")


# from flask import render_template
from app.config import get_cfg
from flask import Response, send_file

# --- -- -

def get_asset(_path):
    try:
        res = send_file("%s%s%s" % (get_cfg("full_path_root"), get_cfg("assets_dir"), _path))
    except Exception, error:
        print "Exception 1433200045:", error
        res = Response(response="Not Found", mimetype="text/plain", status=404)

    return res
    # (END)


# --- -- -

def get_script(_path):
    try:
        res = send_file("%s%s%s" % (get_cfg("full_path_root"), get_cfg("scripts_dir"), _path))
    except Exception, error:
        print "Exception 1433200151:", error
        res = Response(response="Not Found", mimetype="text/plain", status=404)

    return res
    # (END)