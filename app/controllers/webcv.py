# -*- coding: utf-8 -*-

from app.libraries.auth import access
from flask import render_template, request, redirect, url_for, session


@access
def webcv_run():
    return render_template('pages/webcv.tpl')