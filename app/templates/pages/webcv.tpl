{% extends "layouts/adminlte/ui.tpl" %}
{% block title %}{# Tytuł strony #}{% endblock %}
{% block head %}
    {# Własne nagłówki i pliki CSS w sekcji HEAD #}
    <style type="text/css">
        /* Style dla index.tpl */
    </style>
{% endblock %}
{% block content %}

    <!-- Main content -->
    <section class="content">
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" id="bttTab1">Dane osobowe</a></li>
                <li><a href="#tab_2" data-toggle="tab" id="bttTab2">Przebieg kariery</a></li>
                <li><a href="#tab_3" data-toggle="tab" id="bttTab3">Umiejętności</a></li>
                <li><a href="#tab_4" data-toggle="tab" id="bttTab4">O mnie :)</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <dl class="dl-horizontal">
                        <dt>Tytuł zawodowy:</dt>
                        <dd>inżynier</dd>

                        <dt>Imię:</dt>
                        <dd>Adrian</dd>

                        <dt>Nazwisko:</dt>
                        <dd>Gargula</dd>

                        <dt>Data urodzenia:</dt>
                        <dd>24 maj 1989</dd>

                        <dt>Telefon:</dt>
                        <dd>+48 794 304 573</dd>

                        <dt>E-mail:</dt>
                        <dd>{{ mail_encrypt('adriangargula@interia.pl') }}</dd>

                        <dt>Adres zamieszkania:</dt>
                        <dd>ul. Kmietowicza, 30-092 Kraków</dd>

                    </dl>

                    <p>Pobierz skrócone CV jako
                        <a download href="{{ get_cfg('assets_url') }}cv/adrian_gargula_cv.pdf">
                            <small class="label bg-green">PDF</small>
                        </a>
                        <a download href="{{ get_cfg('assets_url') }}cv/adrian_gargula_cv.xps">
                            <small class="label bg-green">XPS</small>
                        </a>
                    </p>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_2">
                    <ul class="timeline timeline-inverse">
                        {# Technikum #}
                        <li class="time-label">
                            <a name="wyksztalcenie"></a>
                            <span class="bg-blue">Wrzesień 2005</span><br/>
                            <span class="bg-gray small">- czerwiec 2009</span>

                        </li>
                        <li>
                            <i class="fa fa-book bg-blue"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-book"></i> Szkoła</span>

                                <h3 class=" timeline-header"><a href="#">Technik Elektronik</a></h3>

                                <div class="timeline-body">
                                    Technikum Zespołu Szkół nr 4 w Nowym Sączu
                                </div>
                            </div>
                        </li>
                        {# / Technikum #}
                        {# Studia #}
                        <li class="time-label">
                            <span class="bg-blue">Październik 2009</span><br/>
                            <span class="bg-gray small">- lipiec 2014</span>
                        </li>
                        <li>
                            <i class="fa fa-book bg-blue"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-book"></i> Szkoła</span>

                                <h3 class="timeline-header"><a href="#">Inżynier Mechatroniki Samochodowej</a></h3>

                                <div class="timeline-body">
                                    Państwowa Wyższa Szkoła Zawodowa w Nowym Sączu
                                </div>
                            </div>
                        </li>
                        {# / Studia #}
                        {# Praca w NETSTEL Software #}
                        <li class="time-label">
                            <a name="praca"></a>
                            <span class="bg-green">Czerwiec 2014</span><br/>
                            <span class="bg-gray small">- sierpień 2014</span>
                        </li>
                        <li>
                            <i class="fa fa-dollar bg-green"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-dollar"></i> Praca</span>

                                <h3 class="timeline-header"><a href="#">Programista PHP/Python</a></h3>

                                <div class="timeline-body">
                                    <b>NETSTEL Software</b> Marcin Matusiak, Spółka Jawna<br/>
                                    ul. Zbożowa 3/18, 30-002 Kraków<br/>
                                    tel. +48 12 633 55 65<br/>
                                    e-mail: admin@netstel.pl
                                </div>

                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs" href="http://netstel.pl/" target="_blank">Strona WWW</a>
                                </div>
                            </div>
                        </li>
                        {# / Praca #}
                        {# Praca 1plus1 Sp. z o.o. #}
                        <li class="time-label">
                            <span class="bg-green">Wrzesień 2014</span><br/>
                            <span class="bg-gray small">- styczeń 2015</span>
                        </li>
                        <li>
                            <i class="fa fa-dollar bg-green"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-dollar"></i> Praca</span>

                                <h3 class="timeline-header"><a href="#">Programista PHP</a></h3>

                                <div class="timeline-body">
                                    <b>1PLUS1</b> Sp. z o.o.<br/>
                                    tel. Tel.: +48 728 392 020</br>
                                    e-mail: bok@1plus1.pl
                                </div>

                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs" href="http://www.1plus1.pl/" target="_blank">Strona WWW</a>
                                </div>
                            </div>
                        </li>
                        {# / Praca #}
                        {# Praca oole.pl #}
                        <li class="time-label">
                            <span class="bg-green">Marzec 2015</span>
                        </li>
                        <li>
                            <i class="fa fa-dollar bg-green"></i>

                            <div class="timeline-item">
                                <span class="time"><i class="fa fa-dollar"></i> Praca</span>

                                <h3 class="timeline-header"><a href="#">Programista JavaScript/Python/PHP</a></h3>

                                <div class="timeline-body">
                                    <b>Grupa Design Plus</b> Sp. z o.o.<br/>
                                    30-835 Kraków, ul. Leonida Teligi 26/37<br/><br/>
                                    <b>Oole!</b> ul. Jerzmanowskiego 37<br/>
                                    tel. 500 41 63 67<br/>
                                    e-mail: info@oole.pl
                                </div>

                                <div class="timeline-footer">
                                    <a class="btn btn-primary btn-xs" href="http://oole.pl/" target="_blank">Strona WWW</a>
                                </div>
                            </div>
                        </li>
                        {# / Praca #}
                        <li>
                            <i class="fa fa-clock-o bg-gray"></i>

                            <div class="timeline-item">
                                <h3 class="timeline-header no-border">...</h3>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_3">
                    {% include 'parts/my-skilss.tpl' %}
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_4">
                    {% include 'parts/about-me.tpl' %}
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->
    </section>
    <!-- /.content -->
{% endblock %}
{% block script %}
    {# Własne skrypty JavaScript #}
{% endblock %}