<!DOCTYPE html>
<html>
<head lang="pl">
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{% block title %}{% endblock %}</title>

    <script type="text/javascript">
        // Inicjalizacja zmiennej globalnej:
        var g = {};
    </script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="{{ get_cfg('vendors_url')+'Bootstrap-3.3.5/css/bootstrap.min.css' }}">
    <link rel="stylesheet" href="{{ get_cfg('vendors_url')+'font-awesome-4.4.0/css/font-awesome.min.css' }}">
    <link rel="stylesheet" href="{{ get_cfg('vendors_url')+'AdminLTE-2.3.0-dist/css/AdminLTE.min.css' }}">
    <link rel="stylesheet" href="{{ get_cfg('css_url')+'style.css' }}">
</head>
<body class="hold-transition lockscreen">

<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
    <div class="lockscreen-logo">
        <a href="#">Web<b>CV</b></a>
    </div>
    <!-- User name -->
    <div class="lockscreen-name">Adrian Gargula</div>

    <!-- START LOCK SCREEN ITEM -->
    <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
            <img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpa1/v/t1.0-1/p160x160/12801_884311604935905_292794708369898331_n.jpg?oh=eb62dbfc0550d1d76ba1b9fa2f9461d5&oe=569A3E3D&__gda__=1453849720_3c02fcdcec32160677076dce791cfe54" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" method="post" action="{{ url_for('lockscreen_run') }}">
            <div class="input-group">
                <input type="password" name="access-code" class="form-control" placeholder="kod dostępu">

                <div class="input-group-btn">
                    <button type="submit" class="btn" name="sign-in" value="run"><i class="fa fa-arrow-right text-muted"></i></button>
                </div>
            </div>
        </form>
        <!-- /.lockscreen credentials -->

    </div>
    <!-- /.lockscreen-item -->
    <div class="help-block text-center">
        Podaj hasło aby zobaczyć CV
    </div>
    <div class="text-center">
        <a href="#">{{ mail_encrypt('adriangargula@interia.pl', _name='Lub skontaktuj sie z autorem') }}</a>
    </div>
    <div class="lockscreen-footer text-center">
        Copyright &copy; 2015 <b>Adrian Gargula</b><br>
        Wszystkie prawa zastrzeżone
    </div>
</div>
<!-- /.center -->

<script type="text/javascript" src="{{ get_cfg('vendors_url')+'jquery-2.1.4.js' }}"></script>
<script type="text/javascript" src="{{ get_cfg('vendors_url')+'Bootstrap-3.3.5/js/bootstrap.min.js' }}"></script>

<script type="text/javascript" src="{{ get_cfg('vendors_url')+'requirejs-2.1.20/require.min.js' }}"></script>
<script type="text/javascript">
    require.config({
        baseUrl: "{{ get_cfg('scripts_url') }}"
    });

    require(['js/mail-decryption-all'], function (_allMailDecrypt) {
        _allMailDecrypt('encrypted-mail');
    });
</script>
</body>
</html>