<div class="box box-widget collapsed-box">
    <div class='box-header with-border'>
        <div class='user-block'>
            <div style="float: left;"><i class="fa fa-paw" style="font-size: 36px;"></i></div>
            <span class='username'>JavaScript</span>
            <span class='description'>jQuery, RequireJS, Modernizr, socket.io, Node.js, NPM, Express, Grunt</span>
        </div>
        <!-- /.user-block -->
        <div class='box-tools'>
            <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class='box-body'>
        <!-- post text -->
        <p>JavaScript jest moim ulubionym językiem programowania ze względu na swoją elastyczność i wszechstronność.
            Dzięki powstaniu Node.js, JavaScript I/O mogę używać JS po stronie serwera, dodatkowo dzięki platformom ELECTRON, node-webkit (NW.js) mam możliwość pisania aplikacji okienkowych na Windows, Linux i Mac OS.</p>

        <p>Oczywiście należny zdawać sobie sprawę iż JS nie nadaje się do pisania wszystkiego. Nie napiszemy w nim sterownika, kodeku, systemu operacyjnego, itp. Chociaż niektóre z tych rzeczy już udało się w JS napisać inne czekają na odważnego programistę ale jest to sztuka dla sztuki. Czemu nie
            napisać jak się da :) Niestety takie rozwiązania nie są mocną stroną JS ze względu to iż jest językiem interpretowanym.</p>

        <p><b>Jak dobrze znam JavaScript.</b> Nie będę reprezentował tutaj procentów ani punktów jak to robią niektórzy - bo przecież 50% to od której strony te 50% języka znasz? Więcej niż mój opis tutaj i zapewnienia pokażą źródła na
            <a href="https://bitbucket.org/Adrosar/" target="_blank">
                <small class="label bg-yellow">BitBucket</small>
            </a>
            i z mojej
            <a href="#piaskownica-1" class="menu-btt">
                <small class="label bg-yellow">Piaskownicy</small>
            </a>
        </p>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        ...
    </div>
    <!-- /.box-footer -->
</div><!-- /.box -->

<div class="box box-widget collapsed-box">
    <div class='box-header with-border'>
        <div class='user-block'>
            <div style="float: left;"><i class="fa fa-paw" style="font-size: 36px;"></i></div>
            <span class='username'>Python</span>
            <span class='description'>Flask, Jinja2, peewee</span>
        </div>
        <!-- /.user-block -->
        <div class='box-tools'>
            <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class='box-body'>
        <!-- post text -->
        <p>Drugi w kolejności ulubiony język. Poznałem go jednak dość późno ale nie zmienia to faktu że przypadł mi do gustu od razu. Jest on również elastyczny i uniwersalny (chociaż nie działa po stronie przeglądarki). nadal można w nim napisać back-end strony/aplikacji internetowej, aplikację
            okienkową a nawet oprogramować mikro-kontroler dzięki MicroPython. Zaleta Pythona jest "wślizgiwanie" się do inny języków i platform: Jython (wcześniej JPython), IronPython, PyPy.
        </p>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        ...
    </div>
    <!-- /.box-footer -->
</div><!-- /.box -->

<div class="box box-widget collapsed-box">
    <div class='box-header with-border'>
        <div class='user-block'>
            <div style="float: left;"><i class="fa fa-paw" style="font-size: 36px;"></i></div>
            <span class='username'>PHP</span>
            <span class='description'>WordPress</span>
        </div>
        <!-- /.user-block -->
        <div class='box-tools'>
            <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class='box-body'>
        <!-- post text -->
        <p>Język który każdy kto tworzy strony czy aplikacje internetowe powinien znać. Jego sił tkwi w bardzo dużej społeczności, ilości dostępnych bibliotek, frameworków, CMS-ów i w ogóle w ilości. <i>Chińczyków też jest dużo ;)</i> No cóż, fakt iż spędziłem przy nim przynajmniej jako początkujący
            developer prawie ta samo dużo
            czasu jak przy ulubionym JavaScrip ale PHP nie jest taki zły gdy już człowiek ogarnie że tego wszystkiego jest za dużo musi się ukierunkować :)</p>

        <p>Ja wybrałem WordPress ze względu iż wielu moich klientów już z niego wcześniej korzystało, był i jest szybki we wdrożeniu i łatwy w modyfikacji.</p>

        <p>Obecnie tworzę skórki i pisze wtyczki od tych banalnych po te które całkowicie odmieniają zastosowanie WordPress'a.</p>

        <p>Wcześniej używałem sporadycznie CakePHP i CodeIgniter.</p>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        ...
    </div>
    <!-- /.box-footer -->
</div><!-- /.box -->

<div class="box box-widget collapsed-box">
    <div class='box-header with-border'>
        <div class='user-block'>
            <div style="float: left;"><i class="fa fa-paw" style="font-size: 36px;"></i></div>
            <span class='username'>HTML & CSS</span>
            <span class='description'>Bootstrap, AdminLTE, RWD, LESS</span>
        </div>
        <!-- /.user-block -->
        <div class='box-tools'>
            <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class='box-body'>
        <!-- post text -->
        <p>Podstawa podstaw przy tworzeniu stron. Nawet nie ma co tu opisywać ...
            No dobra "znam wszystko, jestem guru :)" może nie do końca "czasami coś zapominam ;)".</p>

        <p>A tak na serio to śledzę nowości w obu tych językach i stwierdzam iż CSS3 przeją już dużo zadań które wcześniej wykonywało się przy pomocy JavaScript (głownie jQuery). Przejścia, animacja, zmiana wyglądu po kliknięciu, rozwijanie po kliknięciu a nawet animacja 3D.
            Co do samego standardu HTML5 to łączy się on mocno z JavaScrip'tem.</p>

        <p>Ciekawsze technologie według mnie to Canvas 2D i 3D, WebGL, Push Notifications, File API, Service Worker, WebSockets, Drag and Drop, Local Storage, Sesion Storage, Web SQL, Application Cache, IndexedDB, Obsługa (wideo, audio, kamery, mikrofonu), WebRTC.</p>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        ...
    </div>
    <!-- /.box-footer -->
</div><!-- /.box -->

<div class="box box-widget collapsed-box">
    <div class='box-header with-border'>
        <div class='user-block'>
            <div style="float: left;"><i class="fa fa-paw" style="font-size: 36px;"></i></div>
            <span class='username'>Inne języki programowania</span>
            <span class='description'>Asembler, C/C++, JAVA, LUA, COBOL</span>
        </div>
        <!-- /.user-block -->
        <div class='box-tools'>
            <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class='box-body'>
        <!-- post text -->
        <ul>
            <li><b>Asembler</b> - uczyłem się go w technikum. Programowaliśmy w nim mikro-kontrolery 8051</li>
            <li><b>C/C++</b> - technikum/studia obecnie hobby. Programuję w nim mikro-kontrolery z rodziny AVR (Atmega). Pisałem również aplikacje okienkowe dla Windows'a głownie w czystym WinAPI ale to tylko dla siebie z ciekawości i chęci poznania jak działa system Windows tak bardziej od środka
                :)
            </li>
            <li><b>JAVA</b> - grając w Minecrafta przyszła mi do głowy myśl aby napisać modyfikację ... i tak się nauczyłem JAVY</li>
            <li><b>COBOL</b> - Również dzięki grze w MC, tylko po stronie serwera.</li>
            <li><b>LUA</b> - jak wyżej :)</li>
        </ul>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <p>... i niech mi ktoś powie ze gry ogłupiają.</p>
    </div>
    <!-- /.box-footer -->
</div><!-- /.box -->

<div class="box box-widget collapsed-box">
    <div class='box-header with-border'>
        <div class='user-block'>
            <div style="float: left;"><i class="fa fa-paw" style="font-size: 36px;"></i></div>
            <span class='username'>Elektronika Cyfrowa</span>
            <span class='description'>rogramowanie C/C++, AVR, Arduino</span>
        </div>
        <!-- /.user-block -->
        <div class='box-tools'>
            <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class='box-body'>
        <!-- post text -->
        <p>Ostatnią rzeczą którą umiem z stopniu na tyle wysokim iż zasługuje na osobny BOX to umiejętności elektronika.
            Niestety nie zostaje mi za dużo czasu aby aktywnie rozwijać ta dziedzinę więc pozostaje ona tylko w granicach hobbystycznych.</p>

        <p>Interesuje się wszelkimi nowinkami technicznymi. Sam czasami coś zaprojektuję przy zastosowaniu praformy Arduino, Raspberry Pi. Zapoczątkowały one falę podobnych układów dzięki którym można łatwo tanio i przyjemnie wdrożyć swoje pomysły w życie.</p>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        ...
    </div>
    <!-- /.box-footer -->
</div><!-- /.box -->

<div class="box box-widget collapsed-box">
    <div class='box-header with-border'>
        <div class='user-block'>
            <div style="float: left;"><i class="fa fa-paw" style="font-size: 36px;"></i></div>
            <span class='username'>Narzędzi</span>
            <span class='description'>które stosuje w swojej pracy</span>
        </div>
        <!-- /.user-block -->
        <div class='box-tools'>
            <button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class='box-body'>
        <!-- post text -->
        <ul>
            <li>NetBeans</li>
            <li>PHPStorm</li>
            <li>PyCharm</li>
            <li>Notepad++</li>
            <li>GIT/SVN (wolę GIT'a)</li>
            <li>PuTTY</li>
            <li>Midnight Commander</li>
            <li>FileZilla</li>
            <li>PoEdit</li>
            <li>PhotoShop</li>
            <li>Przeglądarki: Google Chrome, Mozilla Firefox, Firefox Nightly Builds, IE (Edge), Opera (nowa), Opera 16 (stara), Opera Mobile Emulator</li>
            <li>WebServ (Apache, MySQL, PHP)</li>
        </ul>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        ...
    </div>
    <!-- /.box-footer -->
</div><!-- /.box -->
