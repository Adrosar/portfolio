<p>Gdybym miał opisać jednym słowem moje hobby to było by to <i>"tworzenie"</i>.
    Czy to stron internetowych, aplikacji, usług, układu elektronicznego czy budowli w grze :) Zawsze lubiłem i lobie tworzyć coś nowego - odkrywać nieznane. </p>
<p>Programowaniem zainteresowałem się w gimnazjum kiedy to na bawiłem się językiem BASIC na starym Comodore64. Potem przyszedł czas na Visual Basic, gdzie po raz pierwszy stworzyłem aplikacje okienkowe.</p>
<p>Po VB przyszedł czas na C/C++. Pamiętam że podchodziłem kilka razy do nauki tego języka aż w końcu się udało. Potem bawiłem się WinAPI przez pół roku - ktoś powie że stracony czas - a ja powiem że z ciekawości.</p>
<p>W technikum elektronicznym nauczyłem się programować w Asemblerze. programowaliśmy w nim mikro-kontrolery 8051. Jaka była radość gdy na wyświetlaczu pokazywały się cyfry czy napis, albo jak z niewielkiego głośniczka wydobywały się piskliwe dźwięki przypominające jakąś melodię :)<br/>
    W tym samym czasie zainteresowałem się programowaniem AVR-ów w C. Potem przyszła platforma Arduino i programowanie w C++ na AVR-ach stało się łatwiejsze.</p>
<p>
    Pod koniec techniku bardzo fascynowały mnie możliwości rozwijającej się platformy Adobe Flash (wtedy jeszcze Macromedia Flash).
    <br/>Bardzo szybko moja wiedza na ten temat zaczęła się poszerzać, dzięki czemu juz po 6 miesiącach udało mi się stworzyć pierwszą stronę internetową w całości we Flash-u. Była to strona mojej szkoły.
    <br/>W tym czasie po kolei przebrnąłem przez ActionScript 2 aż po ActionScript 3 w Adobe Flash CS3.
</p>
<p>
    Po rozpoczęciu studiów na kierunku "Mechatronika Samochodowa" zacząłem interesować się niedocenianym prze zemnie wtedy JavaScript-em. Jego nauka przyszła mi z łatwością. Pewnie dzięki wcześniejszej znajomości ActionScriptu.
    <br/>Gdy HTML5 i CSS3 zaczęły się coraz częściej przebijać do projektów stron i aplikacji internetowych zdecydowałem że całkowicie odpuszczę sobie platformę Adobe Flash.
</p>
<p>
    Podczas studiów dorabiałem sobie współpracą z lokalną firmą DETEPE Studio z Nowego Sącza, gdzie tworzyłem strony internetowe.
</p>
<p>
    Moje znajomość technologii sieciowych i elektroniki przyczyniły się do wyboru tematu na pracę inżynierska. Projekt miał na celu opracowanie systemu powiadomień GSM o pożarze, włamaniu, itp. Dodatkowo stan czujników można było sprawdzić poprzez usługę WWW. Był to "prosty" moduł dla inteligentnego
    domu
</p>
<p>
    Na ostatnim roku studiów pojawiła się praforma Node.js, potem pochodne od niej node-webkit, ELECTRON , itp. Ta cała gamma platform uczyniła JavaScript potężnym narzędziem w którym zrobimy prawie wszystko.
</p>
<p>
    A gdzie PHP i Python?
</p>
<p>
    PHP-a uczyłem się cały czas, to znaczy sam mi wszedł do głowy :) Dla mnie PHP to podstawowy język jeżeli chce się zacząć tworzyć aplikacje sieciowe. Nie oznacza to jednak że traktuje PHP po macoszemu, wręcz przeciwnie, podczas poznawania kolejnych tajników tego języka bardzo dokładnie
    studiowałem jego problemy, rozwiązania i zastosowanie konkretnych linijek doku.
</p>
<p>
    Natomiast Python-a nauczyłem się zaraz po poznaniu platformy Node.js i o dziwo bardzo przypadł mi do gustu - przyjemny język jak dla mnie.
    Gdzieś na forum wyczytałem że Node.js bardzo swoim zastosowaniem przypomina właśnie Pythona, a że Python jest starszym i dojrzałym językiem (wraz z implementacjami).
    Łatwiej jest znaleźć hosting dla niego, jest popularniejszy i również bardzo uniwersalny. Dodatkowo można go zastosować tam gdzie JavaScript "nie da sobie rady", np MicroPython, PyQt, skrypty na systemach Linux, obliczenia matematyczne, elektronika.
</p>
<a name="piaskownica-1"></a>
<p>
    Dla ciekawskich moich poczynań programistycznych udostępniam pierwszą część mojej <i>"piaskownicy"</i>.
</p>
<div class="callout callout-info">
    <h4>Dla ciekawskich moich poczynań programistycznych udostępniam pierwszą część mojej <i>"piaskownicy"</i>.</h4>
    <dl class="dl-horizontal">
        <dt>Link:</dt>
        <dd>
            <code class="bg-blue">https://mega.nz/#!NYpkCIyZ!S_TK41onZ-cm222XBHQz4P4iEPyPdW4RGpGb6SJahVQ</code>
            <a href="https://mega.nz/#!NYpkCIyZ!S_TK41onZ-cm222XBHQz4P4iEPyPdW4RGpGb6SJahVQ" target="_blank">
                <small class="label pull-right bg-green">Pobierz</small>
            </a>
        </dd>
        <dt>Plik:</dt>
        <dd><code class="bg-blue">adrian-2015-09-29-1332.7z</code></dd>
        <dt>Hasło:</dt>
        <dd><code class="bg-blue">w25vfwj$iqlpUyCjr3j8vVd#h38u9cyR#0L0bcNsbfyn3#Tt$mkp@prEe5qjdsA&</code></dd>
        <dt>MD5:</dt>
        <dd><code class="bg-blue">51B10174883E425B9FB1E84166C4C61C</code></dd>
        <dt>SHA1:</dt>
        <dd><code class="bg-blue">59A481356854C30D8798B5C58574555069BA3745</code></dd>
        <dt>CRC32</dt>
        <dd><code class="bg-blue">1C4C6965</code></dd>
    </dl>
</div>
<p>
    Paczka zawiera moje prywatne projekty, przykłady, rozwiązania itp.
    Źródła pochodzą z czasów studiów, głownie ostatni rok. Z czasem będę starał się wrzucać resztę mojej zabawy z kodem :)
    <br/>Natomiast nowo powstałe projekty będą udostępniane na koncie BitBucket.
</p>