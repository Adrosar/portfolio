<header class="main-header">
    <!-- Logo -->
    <a href="{{ url_for('webcv_run') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>CV</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Web<b>CV</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="{{ url_for('logout_run') }}">Wyloguj się</a>
                </li>
            </ul>
        </div>
    </nav>
</header>