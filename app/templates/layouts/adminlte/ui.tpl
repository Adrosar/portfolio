{% extends "layouts/adminlte/html5.tpl" %}
{% block title %}Adrian Gargula{% endblock %}
{% block head %}
    {# Własne nagłówki i pliki CSS w sekcji HEAD #}
    <style type="text/css">
        /* Style dla index.tpl */
    </style>
{% endblock %}
{% block body %}
    <!-- Site wrapper -->
    <div class="wrapper">

        <!-- Header -->
        {% include "layouts/adminlte/header.tpl" %}

        <!-- Left side column. contains the sidebar -->
        {% include "layouts/adminlte/sidebar.tpl" %}

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            {% block content %}{% endblock %}
        </div>
        <!-- /.content-wrapper -->
        {% include "layouts/adminlte/footer.tpl" %}
    </div>
    <!-- ./wrapper -->
{% endblock %}
{% block script %}
    {# Własne skrypty JavaScript #}
{% endblock %}