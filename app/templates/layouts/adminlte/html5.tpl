<!DOCTYPE html>
<html>
<head lang="pl">
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{% block title %}{% endblock %}</title>

    <script type="text/javascript">
        // Inicjalizacja zmiennej globalnej:
        var g = {};
    </script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="{{ get_cfg('vendors_url')+'Bootstrap-3.3.5/css/bootstrap.min.css' }}">
    <link rel="stylesheet" href="{{ get_cfg('vendors_url')+'font-awesome-4.4.0/css/font-awesome.min.css' }}">
    <link rel="stylesheet" href="{{ get_cfg('vendors_url')+'AdminLTE-2.3.0-dist/css/AdminLTE.min.css' }}">
    <link rel="stylesheet" href="{{ get_cfg('vendors_url')+'AdminLTE-2.3.0-dist/css/skins/_all-skins.min.css' }}">
    <link rel="stylesheet" href="{{ get_cfg('css_url')+'style.css' }}">

    {% block head %}{% endblock %}
</head>
<body class="hold-transition skin-blue sidebar-mini">
{% block body %}{% endblock %}
<script type="text/javascript" src="{{ get_cfg('vendors_url')+'jquery-2.1.4.js' }}"></script>
<script type="text/javascript" src="{{ get_cfg('vendors_url')+'Bootstrap-3.3.5/js/bootstrap.min.js' }}"></script>

<script type="text/javascript" src="{{ get_cfg('vendors_url')+'AdminLTE-2.3.0-plugins/slimScroll/jquery.slimscroll.min.js' }}"></script>
<script type="text/javascript" src="{{ get_cfg('vendors_url')+'AdminLTE-2.3.0-plugins/fastclick/fastclick.min.js' }}"></script>
<script type="text/javascript" src="{{ get_cfg('vendors_url')+'AdminLTE-2.3.0-dist/js/app.min.js' }}"></script>

<script type="text/javascript" src="{{ get_cfg('vendors_url')+'requirejs-2.1.20/require.min.js' }}"></script>
<script type="text/javascript">
    require.config({
        baseUrl: "{{ get_cfg('scripts_url') }}"
    });

    require(['js/main']);
</script>
{% block script %}{% endblock %}
</body>
</html>