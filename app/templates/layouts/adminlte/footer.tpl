<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Copyright &copy; 2015 <b>Adrian Gargula</b> Wszystkie prawa zastrzeżone.
    </div>
    <div class="clear-both"></div>
</footer>