<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpa1/v/t1.0-1/p160x160/12801_884311604935905_292794708369898331_n.jpg?oh=eb62dbfc0550d1d76ba1b9fa2f9461d5&oe=569A3E3D&__gda__=1453849720_3c02fcdcec32160677076dce791cfe54" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Adrian Gargula</p>
                <span><i class="fa fa-envelope"></i> <i>{{ mail_encrypt('adriangargula@interia.pl', _name='Napisz') }}</i></span>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li>
                <a href="#wyksztalcenie" class="menu-btt">
                    <i class="fa fa-book"></i> <span>Wykształcenie</span>
                </a>
            </li>
            <li>
                <a href="#praca" class="menu-btt">
                    <i class="fa fa-wrench"></i> <span>Praca</span>
                </a>
            </li>
            <li>
                <a href="#umiejetnosci" class="menu-btt">
                    <i class="fa fa-check-square"></i> <span>Umiejętności</span>
                </a>
            </li>
            <li>
                <a href="#projekty" class="menu-btt">
                    <i class="fa fa-cubes"></i> <span>Projekty</span>
                </a>
            </li>
        </ul>

        <ul class="sidebar-menu">
            <li class="header">LINKI</li>
            <li>
                <a href="https://bitbucket.org/Adrosar/" target="_blank">
                    <i class="fa fa-bitbucket"></i> <span>BitBucket</span>
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com/adriangargula" target="_blank">
                    <i class="fa fa-facebook"></i> <span>Facebook</span>
                </a>
            </li>
            <li>
                <a href="https://www.linkedin.com/in/adriangargula" target="_blank">
                    <i class="fa fa-linkedin"></i> <span>LinkedIn</span>
                </a>
            </li>
            <li>
                <a href="http://www.goldenline.pl/adrian-gargula" target="_blank">
                    <i class="fa fa-external-link"></i> <span>GoldenLine</span>
                </a>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>